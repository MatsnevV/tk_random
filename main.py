from tkinter import *
from tkinter.ttk import Checkbutton
from tkinter import Menu
from tkinter import ttk
from tkinter import scrolledtext
import random
import secrets
# задание функции

def clickedExit():
    exit()

def clicked():
    res = f"Привет {txt.get()}"
    lbl.configure(text=res)


def clickedCopy():
    res = f"Привет {txt.get()}"
    lbl.configure(text=res)


def clickedGener():
    res = f"Привет {txt.get()}"
    lbl.configure(text=res)


def clickedGenerToken():
    try:
        res = secrets.token_urlsafe(int(txt.get()))
    except ValueError:
        res = 0
    lbl_token.configure(text=res)


#описание формы
window = Tk()
window.title("Добро пожаловать в Генератор паролей")
window.geometry('640x480')

#меню
menu = Menu(window)
new_item = Menu(menu, tearoff=0)
new_item.add_command(label='Генерация', command=clickedGener)
new_item.add_command(label='token', command=clickedGenerToken)
new_item.add_command(label='Copy', command=clickedCopy)
new_item.add_separator()
new_item.add_command(label='Exit', command=clickedExit)
menu.add_cascade(label='Файл', menu=new_item)
window.config(menu=menu)


tab_control = ttk.Notebook(window)
tab_pass = ttk.Frame(tab_control)
tab_log = ttk.Frame(tab_control)

tab_control.add(tab_pass, text='Генератор паролей')
tab_control.add(tab_log, text='Генератор логинов')
#разметка кнопки и тд

chk_state = BooleanVar()
chk_state.set(True)  # задайте проверку состояния чекбокса
chk = Checkbutton(tab_pass, text='Цифры', var=chk_state)
chk.grid(column=0, row=1)

chk_state = BooleanVar()
chk_state.set(True)  # задайте проверку состояния чекбокса
chk = Checkbutton(tab_pass, text='Прописные буквы', var=chk_state)
chk.grid(column=0, row=2)

chk_state = BooleanVar()
chk_state.set(True)  # задайте проверку состояния чекбокса
chk = Checkbutton(tab_pass, text='Строчные буквы', var=chk_state)
chk.grid(column=0, row=3)

chk_state = BooleanVar()
chk_state.set(False)  # задайте проверку состояния чекбокса
chk = Checkbutton(
    tab_pass, text='Спец. символы %, *, ),?, @, #, $, ~', var=chk_state)
chk.grid(column=0, row=4)

lbl = Label(tab_pass, text="Веведи длину пароля", font=(
    "Times New Roman", 14))
lbl.grid(column=0, row=0)

lbl_token = Label(tab_pass, text="", font=(
    "Times New Roman", 14))
lbl_token.grid(column=0, row=5)

txt = Entry(tab_pass, text="8", width=10)
txt.grid(column=1, row=0)

btn = Button(tab_pass, text="кнопка!",  command=clicked)
btn.grid(column=2, row=5)


txt_in = scrolledtext.ScrolledText(tab_log, width=40, height=10)
txt_in.grid(column=0, row=0)

txt_out = scrolledtext.ScrolledText(tab_log, width=40, height=10)
txt_out.grid(column=0, row=2)

tab_control.pack(expand=1, fill='both')
#
window.mainloop()
